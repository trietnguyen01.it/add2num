class MyBigNumber {
    sum(stn1, stn2) {
        let result = '';
        let carry = 0;
        let len1 = stn1.length;
        let len2 = stn2.length;
        let maxlen = Math.max(len1, len2);

        for (let i = 0; i < maxlen; i++) {
            let d1 = i < len1 ? parseInt(stn1[len1 - 1 - i]) : 0;
            let d2 = i < len2 ? parseInt(stn2[len2 - 1 - i]) : 0;

            let sum = d1 + d2 + carry;
            carry = Math.floor(sum / 10);
            result = (sum % 10).toString() + result;

            console.log(`Đợt ${i + 1} :  ${d1} + ${d2} + ${carry} = ${sum}, nhớ: ${carry}`);
        }

        if (carry > 0) {
            result = carry.toString() + result;
            console.log(`Còn nhớ: ${carry}`);
        }

        return result;
    }
}
const myBigNumber = new MyBigNumber();
const result = myBigNumber.sum("9234", "897");
console.log(result);